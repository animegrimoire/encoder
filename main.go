package main

import (
	"log"

	"animegrimoire.moe/encoder/internal/config"
	"animegrimoire.moe/encoder/internal/database"
	"animegrimoire.moe/encoder/internal/encoder"
	"animegrimoire.moe/encoder/internal/helper"
	"animegrimoire.moe/encoder/internal/notify"
	"animegrimoire.moe/encoder/internal/parser"
)

func init() {
	config.LoadConfig()
}

func main() {
	database.InitDB()
	defer database.CloseDB()

	ffprobe := config.GetFFprobePath()

	log.Println("Starting main encoder.")

	sources := helper.ScanSourcesMKV("data/source")
	for _, filename := range sources {
		snapshot := filename
		helper.CreateSnapshot(snapshot)
		helper.CopySource(filename, "data/tmp")
		filename = helper.SourceName

		getBaseName := helper.GetBaseName(filename)
		getFileSize := helper.GetFileSize(filename)

		helper.TestFileName(filename)

		database.LogActivity(getBaseName, getFileSize, "PARSING")
		notify.SendNotify(getBaseName, getFileSize, "PARSING")

		if err := parser.FFprobeDump(ffprobe, filename); err != nil {
			log.Fatalf("Failed to run ffprobe: %v", err)
		}

		parser.ExtractFonts(filename, "/usr/share/fonts/animegrimoire")
		helper.RemoveUnnecessaryData("debug.tmp", "first_phase.tmp")

		videoStream, audioStream, subtitleStream := parser.GuessStreams("first_phase.tmp")
		parser.FFmpegExtractStreams(filename, videoStream, audioStream, subtitleStream, "data/tmp/output.mkv")
		parser.FFmpegExtractSubtitle("data/tmp/output.mkv", subtitleStream, "data/tmp/subtitle.ass")

		helper.RemoveFiles(filename, "first_phase.tmp", "debug.tmp")
		helper.AddWatermark("data/tmp/subtitle.ass")

		parser.FFmpegRemoveSubtitle("data/tmp/output.mkv", "data/tmp/stripped.mkv")
		parser.FFmpegRemuxFile("data/tmp/stripped.mkv", "data/tmp/subtitle.ass", "data/tmp/input.mkv")

		database.LogActivity(getBaseName, getFileSize, "QUEUED")
		notify.SendNotify(getBaseName, getFileSize, "QUEUED")

		encoder.HandBrakeEncode("data/tmp/input.mkv", "data/tmp/output.mp4", getBaseName, getFileSize)
		helper.ArchiveFile("data/tmp/output.mp4", filename)

		database.Sqlite2Html()
		helper.DeleteSnapshot(snapshot)
	}
}
