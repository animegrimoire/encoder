package config

import (
	"log"
	"os/exec"
)

func GetFFmpegPath() string {
	ffmpeg, err := exec.LookPath("/usr/local/bin/ffmpeg")
	if err != nil {
		log.Panic(err)
	}
	log.Println("FFmpeg path: ", ffmpeg)
	return ffmpeg
}

func GetFFprobePath() string {
	ffprobe, err := exec.LookPath("/usr/local/bin/ffprobe")
	if err != nil {
		log.Panic(err)
	}
	log.Println("FFprobe path: ", ffprobe)
	return ffprobe
}

func GetFCCachePath() string {
	fc_cache, err := exec.LookPath("/usr/bin/fc-cache")
	if err != nil {
		log.Panic(err)
	}
	log.Println("FCCache path: ", fc_cache)
	return fc_cache
}

func GetHandBrakeCLIPath() string {
	handbrake, err := exec.LookPath("/usr/local/bin/HandBrakeCLI")
	if err != nil {
		log.Panic(err)
	}
	log.Println("HandBrakeCLI path: ", handbrake)
	return handbrake
}

func GetTreePath() string {
	tree, err := exec.LookPath("/usr/bin/tree")
	if err != nil {
		log.Panic(err)
	}
	log.Println("Tree path: ", tree)
	return tree
}
