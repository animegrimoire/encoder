package config

import (
	"log"
	"os"
	"sync"
	"time"

	"github.com/joho/godotenv"
)

var (
	discordWebhookToken   string
	telegramToken         string
	ephemeralId           string
	notifyEnabled         bool
	discrodNotifyEnabled  bool
	telegramNotifyEnabled bool
	telegramChatID        string
	configLoaded          bool
	replaceFonts          string
	once                  sync.Once
)

func LoadConfig() {
	once.Do(func() {
		err := godotenv.Load("config/animegrimoire.conf")
		if err != nil {
			log.Fatalf("Error loading config/animegrimoire.conf file")
			return
		}

		discordWebhookToken = os.Getenv("discord_webhook_token")
		telegramToken = os.Getenv("telegram_token")
		telegramChatID = os.Getenv("telegram_chat_id")
		ephemeralId = os.Getenv("ephemeral_id")
		notifyEnabled = os.Getenv("notify_enabled") == "true"
		discrodNotifyEnabled = os.Getenv("discord_notify_enabled") == "true"
		telegramNotifyEnabled = os.Getenv("telegram_notify_enabled") == "true"
		replaceFonts = os.Getenv("replace_fonts")
		configLoaded = true
	})
}

func IsConfigLoaded() bool {
	return configLoaded
}

func GetEphemeralId() string {
	return ephemeralId
}

func IsNotifyEnabled() bool {
	return notifyEnabled
}

func GetDiscordWebhookToken() string {
	return discordWebhookToken
}

func GetTelegramToken() string {
	return telegramToken
}

func GetPrettyDate() string {
	get_date := time.Now().UTC().Format("02/01/06 15:04 MST")
	return get_date
}

func GetDiscordNotifyEnabled() bool {
	return discrodNotifyEnabled
}

func GetTelegramNotifyEnabled() bool {
	return telegramNotifyEnabled
}

func GetTelegramChatID() string {
	return telegramChatID
}

func GetReplaceFonts() string {
	return replaceFonts
}
