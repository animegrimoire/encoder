package parser

import (
	"bufio"
	"log"
	"os"
	"strings"
)

func GuessAudioStream(input_location string) string {
	log.Println("Guessing audio stream of", input_location)
	inputFile, err := os.Open(input_location)
	if err != nil {
		log.Panic(err)
	}
	defer inputFile.Close()

	scanner := bufio.NewScanner(inputFile)
	var audioStream string
	var firstAudioStream string

	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, "Audio") {
			if firstAudioStream == "" {
				firstAudioStream = strings.Split(strings.Split(line, "#")[1], ":")[0] + ":" + strings.Split(strings.Split(line, "#")[1], ":")[1][:1]
			}
			if strings.Contains(line, "default") {
				audioStream = strings.Split(strings.Split(line, "#")[1], ":")[0] + ":" + strings.Split(strings.Split(line, "#")[1], ":")[1][:1]
				break
			}
		}
	}

	if err := scanner.Err(); err != nil {
		log.Panic(err)
	}

	if audioStream == "" {
		audioStream = firstAudioStream
	}

	return audioStream
}
