package parser

import (
	"fmt"
	"log"
	"os"
	"os/exec"

	"animegrimoire.moe/encoder/internal/config"
)

func ExtractFonts(input_location string, output_location string) {
	fccache := config.GetFCCachePath()
	log.Printf("Extracting fonts...")
	ffmpeg := config.GetFFmpegPath()

	cwd, err := os.Getwd()
	if err != nil {
		log.Fatalf("Failed to get current working directory: %v", err)
	}
	defer func() {
		if err := os.Chdir(cwd); err != nil {
			log.Fatalf("Failed to change back to original directory: %v", err)
		}
	}()

	if err := os.Chdir(output_location); err != nil {
		log.Fatalf("Failed to change directory to %s: %v", output_location, err)
	}

	cmd := exec.Command("sh", "-c", fmt.Sprintf("%s -dump_attachment:t \"\" -i \"%s\" -y", ffmpeg, input_location))
	log.Println("Extracting fonts from the source file")
	if err := cmd.Run(); err != nil {
		log.Printf("ExtractFonts returned an error: %v, but continuing execution", err)
	}

	cmd = exec.Command("sh", "-c", fmt.Sprintf("%s -f", fccache), "-v")
	log.Println("Running fc-cache")
	if err := cmd.Run(); err != nil {
		log.Fatalf("Failed to run fc-cache: %v", err)
	}

}
