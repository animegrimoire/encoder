package parser

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"

	"animegrimoire.moe/encoder/internal/config"
	"animegrimoire.moe/encoder/internal/helper"
)

func FFprobeDump(ffprobe, filename string) error {
	log.Println("Running FFprobeDump on", filename)
	cmd := exec.Command("sh", "-c", fmt.Sprintf("%s -hide_banner -i \"%s\" 2>./debug.tmp", ffprobe, filename))
	return cmd.Run()
}

func FFmpegExtractStreams(filename string, videoStream string, audioStream string, subtitleStream string, output string) {
	ffmpeg := config.GetFFmpegPath()
	st := subtitleStream[strings.LastIndex(subtitleStream, ":")+1:]
	log.Println("Extracting streams from", filename)
	cmd := fmt.Sprintf("%s -hide_banner -i \"%s\" -map %s -map %s -map %s -c:v copy -c:a copy -c:s:%s copy -metadata:s:a:0 language=jpn -metadata:s:v:0 language=jpn -metadata:s:s:0 language=eng \"%s\" -y", ffmpeg, filename, videoStream, audioStream, subtitleStream, st, output)
	err := exec.Command("sh", "-c", cmd).Run()
	if err != nil {
		log.Panic("FFmpegExtractStreams returned an error. Exiting.")
	}
}

func FFmpegExtractSubtitle(input string, subtitleStream string, output string) {
	ffmpeg := config.GetFFmpegPath()
	cmd := fmt.Sprintf("%s -hide_banner -i \"%s\" -map %s %s", ffmpeg, input, subtitleStream, output)
	log.Println("Extracting subtitle from", input)
	err := exec.Command("sh", "-c", cmd).Run()
	if err != nil {
		log.Panic("FFmpegExtractSubtitle returned an error. Exiting.")
	}
}

func FFmpegRemoveSubtitle(input string, output string) {
	ffmpeg := config.GetFFmpegPath()
	cmd := fmt.Sprintf("%s -hide_banner -i \"%s\" -map 0 -sn -codec copy \"%s\"", ffmpeg, input, output)
	log.Println("Removing subtitle from", input)
	err := exec.Command("sh", "-c", cmd).Run()
	if err != nil {
		fmt.Printf("FFmpegRemoveSubtitle returned an error: %v, but continuing execution\n", err)
	}
	if err := os.Remove(input); err != nil {
		log.Panic("Failed to run remove subtitle file. Exiting.")
	}
}

func GuessStreams(tmpFile string) (string, string, string) {
	log.Println("Guessing streams from", tmpFile)
	videoStream := GuessVideoStream(tmpFile)
	audioStream := GuessAudioStream(tmpFile)
	subtitleStream := GuessSubtitleStream(tmpFile)

	log.Println("Video Stream: ", videoStream)
	log.Println("Audio Stream: ", audioStream)
	log.Println("Subtitle Stream: ", subtitleStream)

	return videoStream, audioStream, subtitleStream
}

func FFmpegRemuxFile(input string, subtitle string, output string) {
	ffmpeg := config.GetFFmpegPath()
	cmd := exec.Command("sh", "-c", fmt.Sprintf("%s -hide_banner -i \"%s\" -i \"%s\" -c:v copy -c:a copy -c:s copy -map 0:0 -map 0:1 -map 1:0 -metadata:s:a:0 language=jpn -metadata:s:v:0 language=jpn -metadata:s:s:0 language=eng \"%s\" -y", ffmpeg, input, subtitle, output))
	log.Println("Remuxing file", input)
	if err := cmd.Run(); err != nil {
		fmt.Printf("FFmpegRemuxFile returned an error: %v, but continuing execution\n", err)
	}
	helper.RemoveFiles(input, subtitle)
}
