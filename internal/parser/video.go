package parser

import (
	"bufio"
	"log"
	"os"
	"strings"
)

func GuessVideoStream(input_location string) string {
	log.Println("Guessing video stream of", input_location)
	inputFile, err := os.Open(input_location)
	if err != nil {
		log.Panic(err)
	}
	defer inputFile.Close()

	scanner := bufio.NewScanner(inputFile)
	var videoStream string
	var firstVideoStream string

	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, "Video") {
			if firstVideoStream == "" {
				firstVideoStream = strings.Split(strings.Split(line, "#")[1], ":")[0] + ":" + strings.Split(strings.Split(line, "#")[1], ":")[1][:1]
			}
			if strings.Contains(line, "default") {
				videoStream = strings.Split(strings.Split(line, "#")[1], ":")[0] + ":" + strings.Split(strings.Split(line, "#")[1], ":")[1][:1]
				break
			}
		}
	}

	if err := scanner.Err(); err != nil {
		log.Panic(err)
	}

	if videoStream == "" {
		videoStream = firstVideoStream
	}

	return videoStream
}
