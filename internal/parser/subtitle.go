package parser

import (
	"bufio"
	"log"
	"os"
	"strings"
)

func GuessSubtitleStream(input_location string) string {
	log.Println("Guessing subtitle stream of", input_location)
	inputFile, err := os.Open(input_location)
	if err != nil {
		log.Panic(err)
	}
	defer inputFile.Close()

	scanner := bufio.NewScanner(inputFile)
	var subtitleStream string
	var firstSubtitleStream string

	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, "Subtitle") {
			if firstSubtitleStream == "" {
				firstSubtitleStream = strings.Split(strings.Split(line, "#")[1], ":")[0] + ":" + strings.Split(strings.Split(line, "#")[1], ":")[1][:1]
			}
			if strings.Contains(line, "eng") && strings.Contains(line, "Full") {
				subtitleStream = strings.Split(strings.Split(line, "#")[1], ":")[0] + ":" + strings.Split(strings.Split(line, "#")[1], ":")[1][:1]
				break
			}
			if strings.Contains(line, "eng") {
				subtitleStream = strings.Split(strings.Split(line, "#")[1], ":")[0] + ":" + strings.Split(strings.Split(line, "#")[1], ":")[1][:1]
			}
			if strings.Contains(line, "default") {
				subtitleStream = strings.Split(strings.Split(line, "#")[1], ":")[0] + ":" + strings.Split(strings.Split(line, "#")[1], ":")[1][:1]
			}
		}
	}

	if err := scanner.Err(); err != nil {
		log.Panic(err)
	}

	if subtitleStream == "" {
		subtitleStream = firstSubtitleStream
	}

	return subtitleStream
}
