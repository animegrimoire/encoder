package database

import (
	"database/sql"
	"log"
	"time"

	"animegrimoire.moe/encoder/internal/config"
	_ "github.com/ncruces/go-sqlite3/driver"
	_ "github.com/ncruces/go-sqlite3/embed"
)

var db *sql.DB
var dataSourceName = "/app/data/database/sqlite3.db"

func InitDB() {
	var err error
	db, err = sql.Open("sqlite3", dataSourceName)
	if err != nil {
		log.Fatalf("Failed to open database: %v", err)
	}

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS activitylog (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			file_name TEXT NOT NULL,
			file_size INTEGER NOT NULL,
			pretty_date TEXT NOT NULL,
			machine_state TEXT NOT NULL,
			hostname TEXT NOT NULL
		);
	`)
	if err != nil {
		log.Fatalf("Failed to create table: %v", err)
	}
}

func CloseDB() {
	if db != nil {
		db.Close()
	}
}

func LogActivity(filename string, filesize int64, state string) {

	prettyDate := time.Now().UTC().Format("02 January 2006 15:04:05 MST")
	hostname := config.GetEphemeralId()

	log.Println("Logging activity.")
	log.Println("Filename:", filename)
	log.Println("Filesize:", filesize)
	log.Println("Pretty date:", prettyDate)
	log.Println("State:", state)
	log.Println("Hostname:", hostname)

	log.Printf("Executing SQL: INSERT INTO activitylog (file_name, file_size, pretty_date, machine_state, hostname) VALUES (%s, %d, %s, %s, %s)", filename, filesize, prettyDate, state, hostname)
	_, err := db.Exec(`
		INSERT INTO activitylog (file_name, file_size, pretty_date, machine_state, hostname)
		VALUES (?, ?, ?, ?, ?)
	`, filename, filesize, prettyDate, state, hostname)
	if err != nil {
		log.Fatalf("Failed to log activity: %v", err)
	}

	log.Println("Activity logged successfully.")
}
