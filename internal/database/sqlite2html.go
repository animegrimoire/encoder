package database

import (
	"database/sql"
	"log"
	"os"
	"strconv"

	_ "github.com/ncruces/go-sqlite3/driver"
	_ "github.com/ncruces/go-sqlite3/embed"
)

func Sqlite2Html() {
	// Open the SQLite database
	db, err := sql.Open("sqlite3", dataSourceName)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Read all content from the table
	rows, err := db.Query("SELECT * FROM activitylog")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	// Create HTML file
	file, err := os.Create("/app/data/html/db.html")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// Write HTML header
	_, err = file.WriteString("<html><head><style>table {border-collapse: collapse;} th, td {border: 1px solid black;padding: 8px;} th {font-weight: bold;} </style></head><body><table>")
	if err != nil {
		log.Fatal(err)
	}

	// Write table header row
	_, err = file.WriteString("<tr>")
	if err != nil {
		log.Fatal(err)
	}
	columns, err := rows.Columns()
	if err != nil {
		log.Fatal(err)
	}
	for _, col := range columns {
		_, err = file.WriteString("<th>" + col + "</th>")
		if err != nil {
			log.Fatal(err)
		}
	}
	_, err = file.WriteString("</tr>")
	if err != nil {
		log.Fatal(err)
	}

	// Write table rows
	for rows.Next() {
		var id int
		var file_name string
		var file_size int
		var pretty_date string
		var machine_state string
		var hostname string

		err = rows.Scan(&id, &file_name, &file_size, &pretty_date, &machine_state, &hostname)
		if err != nil {
			log.Fatal(err)
		}

		_, err = file.WriteString("<tr>")
		if err != nil {
			log.Fatal(err)
		}
		_, err = file.WriteString("<td>" + strconv.Itoa(id) + "</td>")
		if err != nil {
			log.Fatal(err)
		}
		_, err = file.WriteString("<td>" + file_name + "</td>")
		if err != nil {
			log.Fatal(err)
		}
		_, err = file.WriteString("<td>" + strconv.Itoa(file_size) + "</td>")
		if err != nil {
			log.Fatal(err)
		}
		_, err = file.WriteString("<td>" + pretty_date + "</td>")
		if err != nil {
			log.Fatal(err)
		}
		_, err = file.WriteString("<td>" + machine_state + "</td>")
		if err != nil {
			log.Fatal(err)
		}
		_, err = file.WriteString("<td>" + hostname + "</td>")
		if err != nil {
			log.Fatal(err)
		}
		_, err = file.WriteString("</tr>")
		if err != nil {
			log.Fatal(err)
		}
	}

	// Write HTML footer
	_, err = file.WriteString("</table></body></html>")
	if err != nil {
		log.Fatal(err)
	}

	log.Println("HTML table created successfully.")
}
