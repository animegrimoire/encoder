package helper

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
)

var sources_mkv []string
var mkvFiles []string
var SourceName string

func ScanSourcesMKV(input_location string) []string {
	log.Println("Scanning MKV Files")
	err := filepath.Walk(input_location, func(path string, info os.FileInfo, err error) error {
		if filepath.Ext(path) == ".mkv" {
			sources_mkv = append(sources_mkv, path)
		}
		return nil
	})
	if err != nil {
		log.Panic(err)
	}
	if len(sources_mkv) == 0 {
		log.Println("MKV Files not found!")
		return nil
	}
	return sources_mkv
}

func CheckMKV(path string) []string {
	filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() && filepath.Ext(path) == ".mkv" {
			mkvFiles = append(mkvFiles, path)
		}
		return nil
	})
	return mkvFiles
}

func CopySource(file string, output_location string) string {
	log.Println("Copying Source File From", file, "to", output_location)
	f, err := os.Open(file)
	if err != nil {
		log.Panic(err)
	}
	defer f.Close()

	baseName := filepath.Base(file)

	openBracketIndex := strings.LastIndex(baseName, "[")
	if openBracketIndex != -1 {
		baseName = baseName[:openBracketIndex-1] + filepath.Ext(baseName)
	}

	closeBracketIndex := strings.Index(baseName, "]")
	if closeBracketIndex != -1 {
		SourceName = fmt.Sprintf("%s/[Source]%s", output_location, baseName[closeBracketIndex+1:])
	} else {
		SourceName = fmt.Sprintf("%s/%s", output_location, baseName)
	}

	absOutputLocation, err := filepath.Abs(output_location)
	if err != nil {
		log.Panic(err)
	}

	SourceName = filepath.Join(absOutputLocation, filepath.Base(SourceName))

	newFile, err := os.Create(SourceName)
	if err != nil {
		log.Panic(err)
	}
	defer newFile.Close()

	_, err = io.Copy(newFile, f)
	if err != nil {
		log.Panic(err)
	}

	RemoveFile(file)

	return SourceName
}

func RemoveUnnecessaryData(input_location string, output_location string) {
	log.Println("Removing Unnecessary Data")
	inputFile, err := os.Open(input_location)
	if err != nil {
		log.Panic(err)
	}
	defer inputFile.Close()

	tempFile, err := os.Create(output_location)
	if err != nil {
		log.Panic(err)
	}
	defer tempFile.Close()

	scanner := bufio.NewScanner(inputFile)
	writer := bufio.NewWriter(tempFile)

	for scanner.Scan() {
		line := scanner.Text()
		if !strings.Contains(line, "Unsupported") &&
			!strings.Contains(line, "ttf") &&
			!strings.Contains(line, "font") &&
			!strings.Contains(line, "Metadata") &&
			!strings.Contains(line, "filename") &&
			!strings.Contains(line, "STATISTICS") &&
			!strings.Contains(line, "NUMBER") &&
			!strings.Contains(line, "BPS") &&
			!strings.Contains(line, "DURATION") &&
			!strings.Contains(line, "Duration") &&
			!strings.Contains(line, "Input") &&
			!strings.Contains(line, "encoder") &&
			!strings.Contains(line, "ENCODER") &&
			!strings.Contains(line, "creation_time") &&
			!strings.Contains(line, "line") &&
			!strings.Contains(line, "ffprobe") &&
			!strings.Contains(line, "common") &&
			!strings.Contains(line, "nal_unit_type") &&
			!strings.Contains(line, "bytes read") &&
			!strings.Contains(line, "@") &&
			!strings.Contains(line, "Attachment") &&
			!strings.Contains(line, "mimetype") &&
			!strings.Contains(line, "Consider") {
			_, err := writer.WriteString(line + "\n")
			if err != nil {
				log.Panic(err)
			}
		}
	}

	if err := scanner.Err(); err != nil {
		log.Panic(err)
	}

	err = writer.Flush()
	if err != nil {
		log.Panic(err)
	}
}

func GetCurrDirectory() string {
	wd, err := os.Getwd()
	if err != nil {
		log.Panic(err)
	}
	log.Printf("Current Working Directory: %s", wd)
	return wd
}

func RemoveFiles(files ...string) {
	for _, file := range files {
		log.Println("Removing File: ", file)
		if err := os.Remove(file); err != nil {
			log.Fatalf("Failed to remove file %s: %v", file, err)
		}
	}
}

func RemoveFile(file string) {
	log.Println("Removing File: ", file)
	if err := os.Remove(file); err != nil {
		log.Fatalf("Failed to remove file %s: %v", file, err)
	}
}
