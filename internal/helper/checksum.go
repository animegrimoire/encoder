package helper

import (
	"crypto/md5"
	"fmt"
	"hash/crc32"
	"io"
	"log"
	"os"
)

func CRC32File(file string) string {
	f, err := os.Open(file)
	if err != nil {
		log.Panic(err)
	}
	defer f.Close()

	h := crc32.NewIEEE()
	_, err = io.Copy(h, f)
	if err != nil {
		log.Panic(err)
	}
	log.Printf("Checksum of %s is %v", file, fmt.Sprintf("%X", h.Sum32()))
	return fmt.Sprintf("%X", h.Sum32())
}

func InsertCRC32(file string, checksum string) {
	newName := fmt.Sprintf("%s[%s].mp4", file[:len(file)-4], checksum)
	log.Println("Renaming file to", newName)
	err := os.Rename(file, newName)
	if err != nil {
		log.Panic(err)
	}
}

func MD5File(file string) string {
	f, err := os.Open(file)
	if err != nil {
		log.Panic(err)
	}
	defer f.Close()

	h := md5.New()
	_, err = io.Copy(h, f)
	if err != nil {
		log.Panic(err)
	}
	log.Printf("Checksum of %s is %v", file, fmt.Sprintf("%X", h.Sum(nil)))
	return fmt.Sprintf("%X", h.Sum(nil))
}
