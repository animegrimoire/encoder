package helper

import (
	"log"
	"time"
)

func GetSeason() string {
	loc, err := time.LoadLocation("Asia/Tokyo")
	if err != nil {
		log.Panic("Error loading location:", err)
	}
	now := time.Now().In(loc)
	month := now.Month()
	day := now.Day()

	switch {
	case (month == time.January || month == time.February || (month == time.March && day <= 30)) || (month == time.December && day >= 31):
		log.Println("Season: Winter")
		return "Winter"
	case (month == time.April || month == time.May || (month == time.June && day <= 30)):
		log.Println("Season: Spring")
		return "Spring"
	case (month == time.July || month == time.August || (month == time.September && day <= 30)):
		log.Println("Season: Summer")
		return "Summer"
	case (month == time.October || month == time.November || (month == time.December && day <= 30)):
		log.Println("Season: Fall")
		return "Fall"
	}
	log.Println("Season: Unknown season")
	return "Unknown"
}

func GetYear() int {
	log.Println("Getting current year...")
	now := time.Now()
	if now.Month() == time.December && now.Day() == 31 {
		return now.Year() + 1
	}
	return now.Year()
}
