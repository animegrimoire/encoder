package helper

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	"animegrimoire.moe/encoder/internal/database"
	"animegrimoire.moe/encoder/internal/notify"
)

func ArchiveFile(currentfile string, filename string) {
	defer func() {
		if r := recover(); r != nil {
			timestamp := time.Now().Unix()
			newPath := fmt.Sprintf("/app/data/tmp/panic_%d.mp4", timestamp)
			err := CopyFile(currentfile, newPath)
			if err != nil {
				log.Printf("Failed to copy problematic file: %v", err)
			} else {
				log.Printf("Problematic file copied to: %s", newPath)
			}
			log.Fatalf("Recovered from panic: %v", r)
		}
	}()

	log.Println("Archiving file:", currentfile)
	filename = filepath.Base(filename)

	getFileSize := GetFileSize(currentfile)

	source, title, episode, resolution, err := TestFileName(filename)
	if err != nil {
		log.Panic(err)
	}

	log.Println("Source:", source)
	log.Println("Title:", title)
	log.Println("Episode:", episode)
	log.Println("Resolution:", resolution)

	newSource := "[animegrimoire]"
	newResolution := fmt.Sprintf("[%s]", resolution)
	year := GetYear()
	season := GetSeason()

	newFilename := fmt.Sprintf("%s %s - %s %s.mp4", newSource, title, episode, newResolution)
	crc32Value := CRC32File(currentfile)
	finalFilename := fmt.Sprintf("%s[%s].mp4", newFilename[:len(newFilename)-4], crc32Value)
	log.Println("Final filename:", finalFilename)

	destDir := fmt.Sprintf("/app/data/archive/%d/%s/%s", year, season, title)
	err = os.MkdirAll(destDir, os.ModePerm)
	if err != nil {
		log.Panic(err)
	}

	destPath := filepath.Join(destDir, finalFilename)

	// Use CopyFile instead of os.Rename to handle cross-device link error
	err = CopyFile(currentfile, destPath)
	if err != nil {
		log.Panic(err)
	}

	err = os.Remove(currentfile)
	if err != nil {
		log.Panic(err)
	}

	log.Println("File moved to:", destPath)

	CreateHtmlTree("/app/data/archive", year, season)

	database.LogActivity(finalFilename, getFileSize, "ADD")
	notify.SendNotify(finalFilename, getFileSize, "ADD")
}
