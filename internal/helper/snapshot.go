package helper

import (
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
)

func CreateSnapshot(src string) error {
	sourceBaseName := GetBaseName(src)
	log.Println("Creating snapshot:", src)

	sourceFile, err := os.Open(src)
	if err != nil {
		return fmt.Errorf("failed to open source file: %w", err)
	}
	defer sourceFile.Close()

	dst := filepath.Join("data", "tmp", "snapshot", fmt.Sprintf("%s.snapshot", sourceBaseName))
	destFile, err := os.Create(dst)
	if err != nil {
		return fmt.Errorf("failed to create snapshot file: %w", err)
	}
	defer destFile.Close()

	_, err = io.Copy(destFile, sourceFile)
	if err != nil {
		return fmt.Errorf("failed to copy source file to snapshot: %w", err)
	}

	err = destFile.Sync()
	if err != nil {
		return fmt.Errorf("failed to sync snapshot file: %w", err)
	}

	log.Println("Snapshot created successfully:", dst)
	return nil
}

func DeleteSnapshot(src string) error {
	file := GetBaseName(src)
	log.Println("Deleting snapshot:", file)

	err := os.Remove(filepath.Join("data", "tmp", "snapshot", fmt.Sprintf("%s.snapshot", file)))
	if err != nil {
		return fmt.Errorf("failed to delete snapshot file: %w", err)
	}

	log.Println("Snapshot deleted successfully.")
	return nil
}
