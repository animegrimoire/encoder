package helper

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"animegrimoire.moe/encoder/internal/config"
)

var SubtitleStyle string = "Style: Watermark,Worstveld Sling,20,&H00FFFFFF,&H00FFFFFF,&H00FFFFFF,&H00FFFFFF,0,0,0,0,100,100,0,0,1,0,0,9,0,5,0,11"
var SubtitleDialouge string = "Dialogue: 0,0:00:00.00,0:00:03.00,Watermark,,0000,0000,0000,,animegrimoire.moe"

func AddWatermark(input string) {
	log.Println("Adding watermark to subtitle...")
	file, err := os.Open(input)
	if err != nil {
		log.Panic(err)
	}
	defer file.Close()

	tempFile, err := os.Create("data/tmp/subtitle.tmp")
	if err != nil {
		log.Panic(err)
	}
	defer tempFile.Close()

	reader := bufio.NewReader(file)
	writer := bufio.NewWriter(tempFile)

	styleInserted := false
	dialogueInserted := false

	for {
		line, err := reader.ReadString('\n')
		if err != nil && err != io.EOF {
			log.Panic(err)
		}
		if !styleInserted && strings.Contains(line, "Format: Name") {
			line = fmt.Sprintf("%s\n%s\n", line, SubtitleStyle)
			styleInserted = true
		}
		if !dialogueInserted && strings.Contains(line, "Format: Layer") {
			line = fmt.Sprintf("%s\n%s\n", line, SubtitleDialouge)
			dialogueInserted = true
		}
		if _, err := writer.WriteString(line); err != nil {
			log.Panic(err)
		}
		if err == io.EOF {
			break
		}
	}

	if err := writer.Flush(); err != nil {
		log.Panic(err)
	}

	if err := os.Rename("data/tmp/subtitle.tmp", input); err != nil {
		log.Panic(err)
	}

	file, err = os.Open(input)
	if err != nil {
		log.Panic(err)
	}
	defer file.Close()

	reader = bufio.NewReader(file)
	foundStyle := false
	foundDialogue := false

	for {
		line, err := reader.ReadString('\n')
		if err != nil && err != io.EOF {
			log.Panic(err)
		}
		if strings.Contains(line, SubtitleStyle) {
			foundStyle = true
		}
		if strings.Contains(line, SubtitleDialouge) {
			foundDialogue = true
		}
		if foundStyle && foundDialogue {
			break
		}
		if err == io.EOF {
			break
		}
	}

	if !foundStyle || !foundDialogue {
		log.Panic("Failed to insert watermark")
	}
	log.Println("Watermark added successfully")

	ReplaceSubtitleFonts(input)
}

func ReplaceSubtitleFonts(input string) {
	log.Println("Replacing subtitle fonts...")

	file, err := os.Open(input)
	if err != nil {
		log.Panic(err)
	}
	defer file.Close()

	tempFile, err := os.Create("data/tmp/subtitle_fonts.tmp")
	if err != nil {
		log.Panic(err)
	}
	defer tempFile.Close()

	reader := bufio.NewReader(file)
	writer := bufio.NewWriter(tempFile)

	replacements := make(map[string]string)

	replaceFonts := config.GetReplaceFonts()
	if replaceFonts == "" {
		log.Println("No fonts to replace.")
		return
	}

	fonts := []string{"Roboto Medium", "Open Sans Semibold", "Arial"}
	if replaceFonts != "" {
		additionalFonts := strings.Split(replaceFonts, ",")
		for _, font := range additionalFonts {
			font = strings.TrimSpace(font)
			fonts = append(fonts, font)
			log.Println("Configured to replace font:", font)
		}
	}

	sizes := make([]int, 0, 73)
	for i := 8; i <= 80; i++ {
		sizes = append(sizes, i)
	}

	for _, font := range fonts {
		for _, size := range sizes {
			replacements[font+","+fmt.Sprint(size)] = "Impress BT," + fmt.Sprint(size+2)
		}
	}

	for {
		line, err := reader.ReadString('\n')
		if err != nil && err != io.EOF {
			log.Panic(err)
		}
		for oldFont, newFont := range replacements {
			line = strings.ReplaceAll(line, oldFont, newFont)
		}
		if _, err := writer.WriteString(line); err != nil {
			log.Panic(err)
		}
		if err == io.EOF {
			break
		}
	}

	if err := writer.Flush(); err != nil {
		log.Panic(err)
	}

	if err := os.Rename("data/tmp/subtitle_fonts.tmp", input); err != nil {
		log.Panic(err)
	}

	log.Println("Subtitle fonts replaced successfully.")
}
