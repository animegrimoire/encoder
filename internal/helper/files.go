package helper

import (
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"regexp"
)

func GetBaseName(filename string) string {
	log.Println("Getting base name of", filename)
	return filepath.Base(filename)
}

func GetFileSize(filename string) int64 {
	fileInfo, err := os.Stat(filename)
	if err != nil {
		return 0
	}
	log.Println("Getting file size of", filename)
	return fileInfo.Size()
}

func CopyFile(src, dst string) error {
	log.Println("Copying file from", src, "to", dst)
	sourceFile, err := os.Open(src)
	if err != nil {
		return err
	}
	defer sourceFile.Close()

	destFile, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer destFile.Close()

	_, err = io.Copy(destFile, sourceFile)
	if err != nil {
		return err
	}

	err = destFile.Sync()
	if err != nil {
		return err
	}

	return nil
}

func TestFileName(filename string) (string, string, string, string, error) {
	log.Println("Checking filename:", filename)
	filename = filepath.Base(filename)

	re := regexp.MustCompile(`^\[(.*?)\] (.*) - (\d+(?:\.\d+)?(?:v\d+)?) \((.*?)\)\.mkv$`)
	matches := re.FindStringSubmatch(filename)
	if len(matches) != 5 {
		return "", "", "", "", fmt.Errorf("filename format is incorrect")
	}
	log.Println("Filename format is correct.")
	return matches[1], matches[2], matches[3], matches[4], nil
}
