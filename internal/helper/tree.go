package helper

import (
	"fmt"
	"log"
	"os/exec"

	"animegrimoire.moe/encoder/internal/config"
)

func CreateHtmlTree(basePath string, year int, season string) {
	tree := config.GetTreePath()
	log.Printf("Creating HTML Tree for %s/%d/%s", basePath, year, season)
	cmd := fmt.Sprintf("%s -h -H . --nolinks %s/%d/%s -o /app/data/html/tree.html", tree, basePath, year, season)
	err := exec.Command("sh", "-c", cmd).Run()
	if err != nil {
		fmt.Printf("CreateHtmlTree returned an error: %v, but continuing execution\n", err)
	}
}
