package notify

func SendNotify(fileName string, fileSize int64, state string) {
	NotifyDiscord(fileName, fileSize, state)
	NotifyTelegram(fileName, fileSize, state)
}
