package notify

import (
	"fmt"
	"log"
	"strconv"

	"animegrimoire.moe/encoder/internal/config"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

func NotifyTelegram(fileName string, fileSize int64, state string) {

	if !config.IsConfigLoaded() {
		log.Println("Configuration not loaded, skipping notification")
		return
	}
	if !config.IsNotifyEnabled() {
		log.Println("Notification disabled, skipping notification")
		return
	}
	if !config.GetTelegramNotifyEnabled() {
		log.Println("Telegram notification disabled, skipping notification")
		return
	}

	bot, err := tgbotapi.NewBotAPI(config.GetTelegramToken())
	if err != nil {
		log.Fatal(err)
	}

	chatID, err := strconv.ParseInt(config.GetTelegramChatID(), 10, 64)
	if err != nil {
		log.Fatal(err)
	}

	messageText := fmt.Sprintf("%s: `%s`", state, fileName)
	message := tgbotapi.NewMessage(chatID, messageText)
	message.ParseMode = "Markdown"

	_, err = bot.Send(message)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Telegram notification sent.")
}
