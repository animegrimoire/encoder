package notify

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"animegrimoire.moe/encoder/internal/config"
)

func NotifyDiscord(fileName string, fileSize int64, state string) {

	if !config.IsConfigLoaded() {
		log.Println("Configuration not loaded, skipping notification")
		return
	}
	if !config.IsNotifyEnabled() {
		log.Println("Notification disabled, skipping notification")
		return
	}
	if !config.GetDiscordNotifyEnabled() {
		log.Println("Discord notification disabled, skipping notification")
		return
	}

	embed := map[string]interface{}{
		"title":       fmt.Sprintf("**[%s]**", state),
		"description": fmt.Sprintf("%s\n\n%s • %s", fileName, config.GetEphemeralId(), config.GetPrettyDate()),
	}
	payload := map[string]interface{}{
		"embeds": []interface{}{embed},
	}
	payloadBytes, _ := json.Marshal(payload)
	response, err := http.Post(config.GetDiscordWebhookToken(), "application/json", bytes.NewBuffer(payloadBytes))
	if err != nil {
		log.Println("Error posting to Discord webhook:", err)
		return
	}
	defer response.Body.Close()

	log.Println("Discord notification sent.")
}
