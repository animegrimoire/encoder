package encoder

import (
	"fmt"
	"log"
	"os/exec"

	"animegrimoire.moe/encoder/internal/config"
	"animegrimoire.moe/encoder/internal/database"
	"animegrimoire.moe/encoder/internal/helper"
	"animegrimoire.moe/encoder/internal/notify"
)

func HandBrakeEncode(input string, output string, getBaseName string, getFileSize int64) {
	handbrake := config.GetHandBrakeCLIPath()
	cmd := fmt.Sprintf("%s --preset-import-file config/x265_Animegrimoire.json -Z \"x265_Animegrimoire\" -i %s -o %s", handbrake, input, output)
	log.Println("Starting HandBrakeCLI...")
	database.LogActivity(getBaseName, getFileSize, "ENCODING")
	notify.SendNotify(getBaseName, getFileSize, "ENCODING")
	err := exec.Command("sh", "-c", cmd).Run()
	if err != nil {
		log.Panic(err)
	}
	log.Println("HandBrakeCLI finished encoding.")
	database.LogActivity(getBaseName, getFileSize, "ENCODED")
	notify.SendNotify(getBaseName, getFileSize, "ENCODED")
	helper.RemoveFile(input)
}
