#!/usr/bin/env bash
# shellcheck source=/dev/null disable=SC2154
set -u
source /app/config/animegrimoire.conf

lock_check() {
  while [ -s "/app/data/tmp/ephemeral.lock" ]; do
    logs "/app/data/tmp/ephemeral.lock found, please remove it to continue."
    sleep 60
  done
}

logs() {
  printf "%s %s\n" "$(date "+%Y/%m/%d %T")" "$*"
}

main_encoder() {
  find "/app/data/source" -iname "*.mkv" | sort >/app/data/tmp/readmkv.txt
  local read_MKV
  read_MKV=$(grep mkv </app/data/tmp/readmkv.txt | head -n 1)
  if [ -n "$read_MKV" ]; then
    lock_check
    logs "File(s) found in source folder. launching encoding process."
    /usr/local/bin/encoder
  fi
}

# check for required executables
executables=(ffprobe ffmpeg fc-cache HandBrakeCLI encoder tree)

for test in "${executables[@]}"; do
  if ! command -v "$test" &>/dev/null; then
    printf "%s is not installed.\n" "$test"
    exit 1
  else
    printf "%s is ready.\n" "$test"
  fi
done

# start
logs "Updating font cache."
fc-cache -fv

logs "Starting encoder daemon."

cd /app || exit 2

while :; do
  lock_check
  main_encoder
  sleep 60
done
