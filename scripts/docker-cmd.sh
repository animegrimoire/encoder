#!/bin/bash

logs() {
    printf "%s %s\n" "$(date "+%Y/%m/%d %T")" "$*"
}

lock_instance() {
    logs "Locking instance."
    date +%s >/app/data/tmp/ephemeral.lock
}

unlock_instance() {
    logs "Unlocking instance."
    rm -vf /app/data/tmp/ephemeral.lock
}

if [ -z "$1" ]; then
    set -- help
fi

case $1 in
lock)
    lock_instance
    ;;
unlock)
    unlock_instance
    ;;
help)
    logs "Usage: 'docker exec encoder /app/scripts/docker-cmd.sh {lock|unlock}'"
    ;;
*)
    logs "Usage: $0 {lock|unlock}"
    exit 1
    ;;
esac
