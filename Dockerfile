# Core hbcli, ffmpeg, ffprobe
FROM registry.gitlab.com/animegrimoire/core:latest as core

# Stage 1: Golang Builder
FROM golang:1.23.1 AS go_builder

WORKDIR /build

COPY . .

RUN go mod download && \
    go build -o /usr/local/bin/encoder -ldflags "-s -w" .

# Stage 2: Final Image
FROM debian:12-slim

WORKDIR /app

COPY . .
COPY --from=core /usr/local/bin/HandBrakeCLI /usr/local/bin/HandBrakeCLI
COPY --from=core /usr/local/bin/ffmpeg /usr/local/bin/ffmpeg
COPY --from=core /usr/local/bin/ffprobe /usr/local/bin/ffprobe
COPY --from=go_builder /usr/local/bin/encoder /usr/local/bin/encoder
RUN mkdir -p /usr/share/fonts/encoder /usr/share/fonts/animegrimoire
COPY --from=core /usr/share/fonts/encoder/000.ttf /usr/share/fonts/encoder/000.ttf
COPY --from=core /usr/share/fonts/encoder/000.otf /usr/share/fonts/encoder/000.otf

# Install runtime dependencies and cleanup
RUN apt update \
    && apt install --no-install-recommends -y \
    ca-certificates \
    libass9 \
    libmp3lame0 \
    libvpx7 \
    libtheora0 \
    libvorbis0a \
    libvorbisenc2 \
    libx264-164 \
    libxml2 \
    libjansson4 \
    libopus0 \
    libspeex1 \
    libturbojpeg0 \
    libnuma1 \
    fontconfig \
    tree \
    && apt clean \
    && rm -rf /var/lib/apt/lists/*

# Create a user and set permissions
RUN useradd -m user && chown -R user:user /app
RUN chown -R user:user /usr/share/fonts/encoder /usr/share/fonts/animegrimoire

USER user

ENTRYPOINT ["/bin/bash", "/app/scripts/docker-entrypoint.sh"]
