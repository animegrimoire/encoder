set shell := ["bash", "-cu"]

default:
    cat Justfile

# golang chores
tidy:
    go mod tidy
lint:
    go fmt
    go vet
vendor:
    go mod vendor
run:
    go run main.go
build:
    go build -o encoder main.go

# terraform chores
pull:
    docker pull "registry.gitlab.com/animegrimoire/encoder:latest"
plan:
    tofu plan -out "main.state"
apply:
    tofu apply "main.state"
deploy: pull plan apply
destroy:
    tofu destroy
