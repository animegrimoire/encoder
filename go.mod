module animegrimoire.moe/encoder

go 1.23.1

require github.com/joho/godotenv v1.5.1

require (
	github.com/ncruces/go-sqlite3 v0.19.0 // indirect
	github.com/ncruces/julianday v1.0.0 // indirect
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	github.com/tetratelabs/wazero v1.8.1 // indirect
	golang.org/x/sys v0.26.0 // indirect
	gopkg.in/telegram-bot-api.v4 v4.6.4 // indirect
)
