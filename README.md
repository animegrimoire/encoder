<div align="center">
<a href="https://animegrimoire.moe">
<img src="https://i.ibb.co/njzy67z/Animegrimoire-moe.png" alt="animegrimoire.org" height="200" width="200"</img>
</a>
</div>

Home: [https://animegrimoire.moe](https://animegrimoire.moe)<br> 

Old Home: [Web Archive](http://web.archive.org/web/20200203143744/https://animegrimoire.org/showthread.php?tid=1119)<br>

Documentations: [animegrimoire/docs](https://gitlab.com/animegrimoire/docs)<br>

<div align="center">
<p><b>This software and preset do not come with any warranty, and we won't provide technical support. If you're using anything included here, it means you already know your shit.</b>
</p>
</div>

This repository is a new rewrite using go for encoding process. If you're looking for bash version, visit branch [legacy](https://gitlab.com/animegrimoire/encoder/-/tree/legacy).

## Note

1. This encoder meant to be run using docker environment.
2. This encoder requires handbrakecli [with fdk-aac](https://web.archive.org/web/20230912023202/https://old.reddit.com/r/GoroHome/comments/e4xiad/handbrake_with_fdk_aac_build/).
3. You may find some files on other sites like Internet Archive that aren't on [Grimoire Archive](https://gitlab.com/animegrimoire/docs/-/raw/main/Grimoire%20Archive.md). This is due to our storage constraints.

For an anime series to stay on Grimoire Archive, the Aurora Team has set some rules:

- We only store up to 800 GiB.
- We target "G-All Ages" anime.
- We focus on "Action, Supernatural, Military, Adventure, Comedy, Slice of Life and Shoujo Ai" genres.
- We prefer anime with a rating of 8.0 or higher.
- Older anime are usually removed first.

Exceptions include:

- Staff favorites.
- Anime tagged "404:Men Not Found".
- Anime referencing real-world topics (like law, agriculture, pets, etc.), aligning with the "G-All Ages" rule.
- Anime with partial or full CGI, needing special handling due to bitrate issues (for compression and bitstarve research).
